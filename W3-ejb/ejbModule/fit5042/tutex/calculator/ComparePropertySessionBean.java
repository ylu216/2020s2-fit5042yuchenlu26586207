package fit5042.tutex.calculator;

import java.util.ArrayList;

import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty {
	
	private ArrayList<Property> propertyList = new ArrayList<>();

	@Override
	public void addProperty(Property property) {
		// TODO Auto-generated method stub
		propertyList.add(property);
		}

	@Override
	public void removeProperty(Property property) {
		// TODO Auto-generated method stub
		propertyList.remove(property);
	}

	@Override
	public int bestPerRoom() {
		// TODO Auto-generated method stub
	
		Integer bestId = 0;
		int numberOfRooms;
		double price;
		double bestPerRoom=10000000.00;
		for(Property p : propertyList) {
			numberOfRooms = p.getNumberOfBedrooms();
			price = p.getPrice();
			if(price/numberOfRooms < bestPerRoom) {
				bestPerRoom=price/numberOfRooms;
				bestId=p.getPropertyId();
			}
		}
		return bestId;
	}
	
	

}
